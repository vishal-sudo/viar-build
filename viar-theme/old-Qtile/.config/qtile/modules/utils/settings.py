# --==[ Settings ]==--

# General
mod = 'mod4'
terminal = 'konsole'
browser = 'firefox'
file_manager = 'thunar'
font = 'SauceCodePro Nerd Font Medium'
#wallpaper = '~/wallpapers/wp1.png'

wallpaper = '/usr/share/wallpapers/viar-wallpaper-n4.jpg'

# Weather
location = {'Mexico': 'Mexico'}
city = 'Mexico City, MX'

# Hardware [/sys/class]
net = 'wlp2s0'
backlight = 'radeon_bl0'

# Color Schemes
colorscheme = 'material_ocean'
