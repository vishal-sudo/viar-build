## Useful aliases
#alias ls='exa -al --color=always --group-directories-first' # preferred listing
#alias la='exa -a --color=always --group-directories-first'  # all files and dirs
#alias ll='exa -l --color=always --group-directories-first'  # long format
#alias lt='exa -aT --color=always --group-directories-first' # tree listing
#alias l.="exa -a | egrep '^\.'"
#-----------------------------------------------------------------------#
alias aup="pamac upgrade --aur"
alias update-grub="grub-mkconfig -o /boot/grub/grub.cfg"
alias unlock="sudo rm /var/lib/pacman/db.lck"
#-----------------------------------------------------------------------#
alias tarnow='tar -acf '
alias untar='tar -zxvf '
alias wget='wget -c '
alias psmem='ps auxf | sort -nr -k 4'
alias psmem10='ps auxf | sort -nr -k 4 | head -10'
alias upd='sudo reflector --latest 5 --age 2 --fastest 5 --protocol https --sort rate --save /etc/pacman.d/mirrorlist && cat /etc/pacman.d/mirrorlist && sudo pacman -Syu && fish_update_completions'
alias orp='pacman -Qtdq | pacman -Rns -'
alias gt='git clone'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias hw='hwinfo --short'                                   #Hardware Info
alias big="expac -H M '%m\t%n' | sort -h | nl"
alias neofetch="neofetch | lolcat"
#Sort installed packages according to size in MB (expac must be installed)
#get fastest mirrors 
alias mirror="sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist" 
alias mirrord="sudo reflector --latest 50 --number 20 --sort delay --save /etc/pacman.d/mirrorlist" 
alias mirrors="sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist" 
alias mirrora="sudo reflector --latest 50 --number 20 --sort age --save /etc/pacman.d/mirrorlist"

export QT_QPA_PLATFORMTHEME=qt5ct


