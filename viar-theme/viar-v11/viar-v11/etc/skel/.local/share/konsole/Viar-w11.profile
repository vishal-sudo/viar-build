[Appearance]
ColorScheme=New Color Scheme

[Cursor Options]
CursorShape=2

[General]
Command=/bin/fish
DimWhenInactive=false
Name=Viar-w11
Parent=FALLBACK/

[Scrolling]
HistoryMode=2

[Terminal Features]
BlinkingCursorEnabled=true
