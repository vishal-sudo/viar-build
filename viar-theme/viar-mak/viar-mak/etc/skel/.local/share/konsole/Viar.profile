[Appearance]
ColorScheme=GreenOnBlack

[Cursor Options]
CursorShape=2

[General]
Command=/bin/fish
DimWhenInactive=false
Name=Viar
Parent=FALLBACK/

[Scrolling]
HistoryMode=2

[Terminal Features]
BlinkingCursorEnabled=true
