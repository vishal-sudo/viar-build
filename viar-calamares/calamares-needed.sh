# !/bin/bash

name=( "kconfig"
       "kcoreaddons "
       "kiconthemes"
       "ki18n "
       "kio "
       "solid "
       "yaml-cpp "
       "kpmcore "
       "mkinitcpio-openswap "
       "boost-libs "
       "ckbcomp "
       "hwinfo "
       "qt5-svg "
       "polkit-qt5 "
       "gtk-update-icon-cache "
       "plasma-framework "
       "qt5-xmlpatterns "
       "squashfs-tools "
       "libpwquality "
       "efibootmgr "
       "icu "
       "extra-cmake-modules "
       "qt5-tools "
       "qt5-translations "
       "git "
       "boost "
       "archiso "
      )
for pkgname in "${name[@]}"; do
		paru -S --noconfirm --needed ${pkgname}
	done

updpkgsums

makepkg -f
































