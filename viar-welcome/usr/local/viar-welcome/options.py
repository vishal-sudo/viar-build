# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'option.ui'
#
# Created by: PyQt5 UI code generator 5.15.4
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(311, 124)
        Dialog.setMaximumSize(QtCore.QSize(311, 124))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/bt-icons/images/viar-icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Dialog.setWindowIcon(icon)
        self.Onlinebtn = QtWidgets.QToolButton(Dialog)
        self.Onlinebtn.setGeometry(QtCore.QRect(10, 30, 132, 81))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.Onlinebtn.sizePolicy().hasHeightForWidth())
        self.Onlinebtn.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setBold(False)
        font.setWeight(50)
        font.setKerning(True)
        self.Onlinebtn.setFont(font)
        self.Onlinebtn.setAutoFillBackground(False)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/bt-icons/images/online.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.Onlinebtn.setIcon(icon1)
        self.Onlinebtn.setIconSize(QtCore.QSize(42, 42))
        self.Onlinebtn.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.Onlinebtn.setAutoRaise(True)
        self.Onlinebtn.setObjectName("Onlinebtn")
        self.Offlinebtn = QtWidgets.QToolButton(Dialog)
        self.Offlinebtn.setGeometry(QtCore.QRect(170, 30, 132, 81))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.Offlinebtn.sizePolicy().hasHeightForWidth())
        self.Offlinebtn.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setBold(False)
        font.setWeight(50)
        font.setKerning(True)
        self.Offlinebtn.setFont(font)
        self.Offlinebtn.setAutoFillBackground(False)
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(":/bt-icons/images/offline.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.Offlinebtn.setIcon(icon2)
        self.Offlinebtn.setIconSize(QtCore.QSize(42, 42))
        self.Offlinebtn.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.Offlinebtn.setAutoRaise(True)
        self.Offlinebtn.setObjectName("Offlinebtn")

        self.retranslateUi(Dialog)
        self.Onlinebtn.clicked.connect(self.online)
        self.Onlinebtn.clicked.connect(Dialog.close)
        self.Offlinebtn.clicked.connect(self.offline)
        self.Offlinebtn.clicked.connect(Dialog.close)

        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Choose Installer"))
        self.Onlinebtn.setText(_translate("Dialog", "Online"))
        self.Offlinebtn.setText(_translate("Dialog", "Offline"))

    def online(self, widget):
        subprocess.call(["sudo", "pacman", "-Sy", "--noconfirm", "viar-calamares-conf", "viar-scripts"],)
        subprocess.call(["sudo", "cp", "/etc/calamares/settings-online.conf", "/etc/calamares/settings.conf"],)
        subprocess.Popen(["/usr/bin/calamares_polkit"], shell=False)

    def offline(self, widget):
        subprocess.call(["sudo", "cp", "/etc/calamares/settings-offline.conf", "/etc/calamares/settings.conf"],)
        subprocess.Popen(["/usr/bin/calamares_polkit"], shell=False)
        #subprocess.call(["sudo","calamares"])

import icons

if __name__ == "__main__":
    import sys
    import subprocess
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())
