# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Viar-Tools.ui'
#
# Created by: PyQt5 UI code generator 5.15.6
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMessageBox

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(480, 528)
        Dialog.setMinimumSize(QtCore.QSize(480, 528))
        Dialog.setMaximumSize(QtCore.QSize(480, 528))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/bt-icons/images/viar-icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Dialog.setWindowIcon(icon)

        self.viarinstaller = QtWidgets.QPushButton(Dialog)
        self.viarinstaller.setGeometry(QtCore.QRect(140, 390, 196, 58))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.viarinstaller.sizePolicy().hasHeightForWidth())
        self.viarinstaller.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setBold(False)
        font.setWeight(50)
        font.setKerning(True)
        self.viarinstaller.setFont(font)
        self.viarinstaller.setAutoFillBackground(False)
        icon = QtGui.QIcon.fromTheme("system-installer")
        self.viarinstaller.setIcon(icon)
        self.viarinstaller.setIconSize(QtCore.QSize(42, 42))
        self.viarinstaller.setObjectName("viarinstaller")
        self.tabWidget = QtWidgets.QTabWidget(Dialog)
        self.tabWidget.setGeometry(QtCore.QRect(20, 120, 441, 261))
        self.tabWidget.setObjectName("tabWidget")
        self.tab_3 = QtWidgets.QWidget()
        self.tab_3.setObjectName("tab_3")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.tab_3)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.groupBox_2 = QtWidgets.QGroupBox(self.tab_3)
        self.groupBox_2.setObjectName("groupBox_2")
        self.gridLayout_5 = QtWidgets.QGridLayout(self.groupBox_2)
        self.gridLayout_5.setObjectName("gridLayout_5")

        self.updatemirror = QtWidgets.QToolButton(self.groupBox_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.updatemirror.sizePolicy().hasHeightForWidth())
        self.updatemirror.setSizePolicy(sizePolicy)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/bt-icons/images/refresh.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.updatemirror.setIcon(icon1)
        self.updatemirror.setIconSize(QtCore.QSize(42, 42))
        self.updatemirror.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.updatemirror.setAutoRaise(True)
        self.updatemirror.setObjectName("updatemirror")
        self.gridLayout_5.addWidget(self.updatemirror, 0, 1, 1, 1, QtCore.Qt.AlignHCenter)

        self.resetlck = QtWidgets.QToolButton(self.groupBox_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.resetlck.sizePolicy().hasHeightForWidth())
        self.resetlck.setSizePolicy(sizePolicy)
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(":/bt-icons/images/reset.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.resetlck.setIcon(icon2)
        self.resetlck.setIconSize(QtCore.QSize(42, 42))
        self.resetlck.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.resetlck.setAutoRaise(True)
        self.resetlck.setObjectName("resetlck")
        self.gridLayout_5.addWidget(self.resetlck, 1, 0, 1, 1, QtCore.Qt.AlignHCenter)

        self.orphans = QtWidgets.QToolButton(self.groupBox_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.orphans.sizePolicy().hasHeightForWidth())
        self.orphans.setSizePolicy(sizePolicy)
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(":/bt-icons/images/trash.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.orphans.setIcon(icon3)
        self.orphans.setIconSize(QtCore.QSize(42, 42))
        self.orphans.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.orphans.setAutoRaise(True)
        self.orphans.setObjectName("orphans")
        self.gridLayout_5.addWidget(self.orphans, 1, 1, 1, 1, QtCore.Qt.AlignHCenter)

        self.systemupdate = QtWidgets.QToolButton(self.groupBox_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.systemupdate.sizePolicy().hasHeightForWidth())
        self.systemupdate.setSizePolicy(sizePolicy)
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap(":/bt-icons/images/update.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.systemupdate.setIcon(icon4)
        self.systemupdate.setIconSize(QtCore.QSize(65, 58))
        self.systemupdate.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.systemupdate.setAutoRaise(True)
        self.systemupdate.setObjectName("systemupdate")
        self.gridLayout_5.addWidget(self.systemupdate, 0, 0, 1, 1, QtCore.Qt.AlignHCenter)

        self.clrcache = QtWidgets.QToolButton(self.groupBox_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.clrcache.sizePolicy().hasHeightForWidth())
        self.clrcache.setSizePolicy(sizePolicy)
        icon5 = QtGui.QIcon()
        icon5.addPixmap(QtGui.QPixmap(":/bt-icons/images/images.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.clrcache.setIcon(icon5)
        self.clrcache.setIconSize(QtCore.QSize(42, 42))
        self.clrcache.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.clrcache.setAutoRaise(True)
        self.clrcache.setObjectName("clrcache")
        self.gridLayout_5.addWidget(self.clrcache, 1, 3, 1, 1, QtCore.Qt.AlignHCenter)

        self.updatekeyring = QtWidgets.QToolButton(self.groupBox_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.updatekeyring.sizePolicy().hasHeightForWidth())
        self.updatekeyring.setSizePolicy(sizePolicy)
        icon6 = QtGui.QIcon()
        icon6.addPixmap(QtGui.QPixmap(":/bt-icons/images/sync.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.updatekeyring.setIcon(icon6)
        self.updatekeyring.setIconSize(QtCore.QSize(42, 42))
        self.updatekeyring.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.updatekeyring.setAutoRaise(True)
        self.updatekeyring.setObjectName("updatekeyring")
        self.gridLayout_5.addWidget(self.updatekeyring, 0, 3, 1, 1, QtCore.Qt.AlignHCenter)

        self.gridLayout_3.addWidget(self.groupBox_2, 0, 1, 1, 1)
        self.tabWidget.addTab(self.tab_3, "")
        self.tab = QtWidgets.QWidget()
        self.tab.setObjectName("tab")
        self.gridLayout_7 = QtWidgets.QGridLayout(self.tab)
        self.gridLayout_7.setObjectName("gridLayout_7")
        self.groupBox = QtWidgets.QGroupBox(self.tab)
        self.groupBox.setObjectName("groupBox")
        self.gridLayout_6 = QtWidgets.QGridLayout(self.groupBox)
        self.gridLayout_6.setObjectName("gridLayout_6")

        self.buttonPartition_2 = QtWidgets.QToolButton(self.groupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.buttonPartition_2.sizePolicy().hasHeightForWidth())
        self.buttonPartition_2.setSizePolicy(sizePolicy)
        icon7 = QtGui.QIcon()
        icon7.addPixmap(QtGui.QPixmap(":/bt-icons/images/Partition_Manager.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.buttonPartition_2.setIcon(icon7)
        self.buttonPartition_2.setIconSize(QtCore.QSize(42, 42))
        self.buttonPartition_2.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.buttonPartition_2.setAutoRaise(True)
        self.buttonPartition_2.setObjectName("buttonPartition_2")
        self.gridLayout_6.addWidget(self.buttonPartition_2, 2, 0, 1, 1, QtCore.Qt.AlignHCenter)

        self.buttonResetsettings_2 = QtWidgets.QToolButton(self.groupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.buttonResetsettings_2.sizePolicy().hasHeightForWidth())
        self.buttonResetsettings_2.setSizePolicy(sizePolicy)
        self.buttonResetsettings_2.setIcon(icon2)
        self.buttonResetsettings_2.setIconSize(QtCore.QSize(42, 42))
        self.buttonResetsettings_2.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.buttonResetsettings_2.setAutoRaise(True)
        self.buttonResetsettings_2.setObjectName("buttonResetsettings_2")
        self.gridLayout_6.addWidget(self.buttonResetsettings_2, 2, 3, 1, 1)

        self.buttonSoftware_2 = QtWidgets.QToolButton(self.groupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.buttonSoftware_2.sizePolicy().hasHeightForWidth())
        self.buttonSoftware_2.setSizePolicy(sizePolicy)
        icon8 = QtGui.QIcon()
        icon8.addPixmap(QtGui.QPixmap(":/bt-icons/images/store.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.buttonSoftware_2.setIcon(icon8)
        self.buttonSoftware_2.setIconSize(QtCore.QSize(42, 42))
        self.buttonSoftware_2.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.buttonSoftware_2.setAutoRaise(True)
        self.buttonSoftware_2.setObjectName("buttonSoftware_2")
        self.gridLayout_6.addWidget(self.buttonSoftware_2, 2, 2, 1, 1, QtCore.Qt.AlignHCenter)

        self.buttonSystemCleaner_2 = QtWidgets.QToolButton(self.groupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.buttonSystemCleaner_2.sizePolicy().hasHeightForWidth())
        self.buttonSystemCleaner_2.setSizePolicy(sizePolicy)
        icon9 = QtGui.QIcon()
        icon9.addPixmap(QtGui.QPixmap(":/bt-icons/images/sweeper.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.buttonSystemCleaner_2.setIcon(icon9)
        self.buttonSystemCleaner_2.setIconSize(QtCore.QSize(42, 42))
        self.buttonSystemCleaner_2.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.buttonSystemCleaner_2.setAutoRaise(True)
        self.buttonSystemCleaner_2.setObjectName("buttonSystemCleaner_2")
        self.gridLayout_6.addWidget(self.buttonSystemCleaner_2, 3, 0, 1, 1)

        self.buttonTimeshift_2 = QtWidgets.QToolButton(self.groupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.buttonTimeshift_2.sizePolicy().hasHeightForWidth())
        self.buttonTimeshift_2.setSizePolicy(sizePolicy)
        icon10 = QtGui.QIcon()
        icon10.addPixmap(QtGui.QPixmap(":/bt-icons/images/timeshift.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.buttonTimeshift_2.setIcon(icon10)
        self.buttonTimeshift_2.setIconSize(QtCore.QSize(42, 42))
        self.buttonTimeshift_2.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.buttonTimeshift_2.setAutoRaise(True)
        self.buttonTimeshift_2.setObjectName("buttonTimeshift_2")
        self.gridLayout_6.addWidget(self.buttonTimeshift_2, 3, 2, 1, 1, QtCore.Qt.AlignHCenter)

        self.buttonDisplay_2 = QtWidgets.QToolButton(self.groupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.buttonDisplay_2.sizePolicy().hasHeightForWidth())
        self.buttonDisplay_2.setSizePolicy(sizePolicy)
        icon11 = QtGui.QIcon()
        icon11.addPixmap(QtGui.QPixmap(":/bt-icons/images/screen.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
       #self.labelAbout.setPixmap(QtGui.QPixmap(":/icons/logo.png"))
       #icon = QtGui.QIcon.fromTheme("terminator")
        self.buttonDisplay_2.setIcon(icon11)
        self.buttonDisplay_2.setIconSize(QtCore.QSize(42, 42))
        self.buttonDisplay_2.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.buttonDisplay_2.setAutoRaise(True)
        self.buttonDisplay_2.setObjectName("buttonDisplay_2")
        self.gridLayout_6.addWidget(self.buttonDisplay_2, 3, 3, 1, 1, QtCore.Qt.AlignHCenter)

        self.gridLayout_7.addWidget(self.groupBox, 0, 0, 1, 1)

        self.tabWidget.addTab(self.tab, "")
        self.labelTitle = QtWidgets.QLabel(Dialog)
        self.labelTitle.setGeometry(QtCore.QRect(-4, 2, 479, 61))
        font = QtGui.QFont()
        font.setFamily("Tillana Medium")
        font.setPointSize(36)
        self.labelTitle.setFont(font)
        self.labelTitle.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.labelTitle.setAlignment(QtCore.Qt.AlignCenter)
        self.labelTitle.setObjectName("labelTitle")
        self.labelIntro = QtWidgets.QLabel(Dialog)
        self.labelIntro.setGeometry(QtCore.QRect(0, 70, 479, 20))
        self.labelIntro.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.labelIntro.setWordWrap(True)
        self.labelIntro.setObjectName("labelIntro")
#      self.checkBox = QtWidgets.QCheckBox(Dialog)
#      self.checkBox.setGeometry(QtCore.QRect(144, 450, 191, 28))
#      self.checkBox.setObjectName("checkBox")

        self.buttonCancel = QtWidgets.QPushButton(Dialog)
        self.buttonCancel.setGeometry(QtCore.QRect(353, 481, 84, 34))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.buttonCancel.sizePolicy().hasHeightForWidth())
        self.buttonCancel.setSizePolicy(sizePolicy)
        icon = QtGui.QIcon.fromTheme("window-close")
        self.buttonCancel.setIcon(icon)
        self.buttonCancel.setAutoDefault(True)
        self.buttonCancel.setObjectName("buttonCancel")

        self.buttonAbout = QtWidgets.QPushButton(Dialog)
        self.buttonAbout.setGeometry(QtCore.QRect(41, 481, 90, 34))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.buttonAbout.sizePolicy().hasHeightForWidth())
        self.buttonAbout.setSizePolicy(sizePolicy)
        self.buttonAbout.setWhatsThis("")
        icon = QtGui.QIcon.fromTheme("help-about")
        self.buttonAbout.setIcon(icon)
        self.buttonAbout.setAutoDefault(True)
        self.buttonAbout.setDefault(True)
        self.buttonAbout.setObjectName("buttonAbout")
        self.labelAbout = QtWidgets.QLabel(Dialog)
        self.labelAbout.setGeometry(QtCore.QRect(236, 482, 16, 18))
        self.labelAbout.setMaximumSize(QtCore.QSize(32, 32))
        self.labelAbout.setMidLineWidth(0)
        self.labelAbout.setText("")
        self.labelAbout.setPixmap(QtGui.QPixmap(":/icons/logo.png"))
        self.labelAbout.setScaledContents(True)
        self.labelAbout.setObjectName("labelAbout")

        self.autostart_dis = QtWidgets.QToolButton(Dialog)
        self.autostart_dis.setGeometry(QtCore.QRect(140, 460, 191, 31))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.autostart_dis.sizePolicy().hasHeightForWidth())
        self.autostart_dis.setSizePolicy(sizePolicy)
        self.autostart_dis.setIconSize(QtCore.QSize(65, 58))
        self.autostart_dis.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.autostart_dis.setAutoRaise(True)
        self.autostart_dis.setObjectName("autostart_dis")

        self.retranslateUi(Dialog)
        self.tabWidget.setCurrentIndex(0)
        self.buttonCancel.clicked.connect(Dialog.close)
        self.buttonAbout.clicked.connect(self.about)
        self.systemupdate.clicked.connect(self.sysUpdate)
        self.updatemirror.clicked.connect(self.refMirror)
        self.updatekeyring.clicked.connect(self.refKeyring)
        self.resetlck.clicked.connect(self.removeLck)
        self.orphans.clicked.connect(self.removeOrphan)
        self.clrcache.clicked.connect(self.removemCache)
        self.buttonPartition_2.clicked.connect(self.partionManager)
        self.buttonSoftware_2.clicked.connect(self.pamac)
        self.buttonResetsettings_2.clicked.connect(self.resetconf)
        self.buttonSystemCleaner_2.clicked.connect(self.syscleaner)
        self.buttonTimeshift_2.clicked.connect(self.restore)
        self.buttonDisplay_2.clicked.connect(self.display)
        self.viarinstaller.clicked.connect(self.installviar)
        self.autostart_dis.clicked.connect(self.autostart)

        #self.viarinstaller.clicked.connect(Dialog.close)
       # self.checkBox.setChecked(True)
         #if self.checkBox.isChecked() == True:
 	 #      self.autostart
 	#     self.buttonCancel.clicked.connect(self.autostart)

        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Viar-OS Welcome"))
        self.viarinstaller.setText(_translate("Dialog", "Install Viar Linux"))
        self.groupBox_2.setTitle(_translate("Dialog", "Viar-OS"))
        self.updatemirror.setText(_translate("Dialog", "Update Mirrorlist"))
        self.resetlck.setText(_translate("Dialog", "Remove Databases \n"
"lock"))
        self.orphans.setText(_translate("Dialog", "Remove Orphan"))
        self.systemupdate.setText(_translate("Dialog", "System Update"))
        self.clrcache.setText(_translate("Dialog", "Clear Cache"))
        self.updatekeyring.setText(_translate("Dialog", "Update Keyrings"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_3), _translate("Dialog", "Start tools"))
        self.groupBox.setTitle(_translate("Dialog", "Extra"))
        self.buttonPartition_2.setText(_translate("Dialog", "Partition Manager"))
        self.buttonResetsettings_2.setText(_translate("Dialog", "Reset Conf"))
        self.buttonSoftware_2.setText(_translate("Dialog", "Add/Remove \n"
"Software"))
        self.buttonSystemCleaner_2.setText(_translate("Dialog", "System Cleaner"))
        self.buttonTimeshift_2.setText(_translate("Dialog", "Restore Snapshot"))
        self.buttonDisplay_2.setText(_translate("Dialog", "Display"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), _translate("Dialog", "Explore"))
        self.labelTitle.setText(_translate("Dialog", "<html><head/><body><p align=\"center\"><span style=\" font-weight:600;\">Viar-OS</span></p></body></html>"))
        self.labelIntro.setText(_translate("Dialog", "<html><head/><body><p align=\"center\"><span style=\" font-size:11pt;\">A Simple, fast, friendly and performance focused OS.</span></p></body></html>"))
#       self.checkBox.setText(_translate("Dialog", "Show this dialog at start up"))
        self.buttonCancel.setToolTip(_translate("Dialog", "Quit application"))
        self.buttonCancel.setText(_translate("Dialog", "Close"))
        self.buttonCancel.setShortcut(_translate("Dialog", "Alt+N"))
        self.buttonAbout.setToolTip(_translate("Dialog", "About this application"))
        self.buttonAbout.setText(_translate("Dialog", "About"))
        self.buttonAbout.setShortcut(_translate("Dialog", "Alt+B"))
        self.autostart_dis.setText(_translate("Dialog", "Click here to disable autostart"))

    def show_popup(self,val):
        msg=QMessageBox()
        msg.setWindowTitle("Report")
        msg.setText(val)
        msg.exec_()
#        Dialog.close()
    def sysUpdate(self, widget):
        Dialog.hide()
        os.system("konsole -e 'bash /usr/local/viar-welcome/scripts/update.sh'")
        Dialog.show()
        self.show_popup("Task Done Sucessfully  ")
    def refMirror(self, widget):
        Dialog.hide()
        os.system("konsole -e 'bash /usr/local/viar-welcome/scripts/mirrors.sh'")
        Dialog.show()
        self.show_popup("Task Done Sucessfully  ")
    def refKeyring(self, widget):
        Dialog.hide()
        os.system("konsole -e 'bash /usr/local/viar-welcome/scripts/keyrings.sh'")
        Dialog.show()
        self.show_popup("Task Done Sucessfully  ")
    def removeLck(self, widget):
        Dialog.hide()
        os.system("konsole -e 'bash /usr/local/viar-welcome/scripts/lock.sh'")
        Dialog.show()
        self.show_popup("Task Done Sucessfully  ")
      # subprocess.call(["sudo","rm","/var/lib/pacman/db.lck"])
    def removeOrphan(self, widget):
        Dialog.hide()
        os.system("konsole -e 'bash /usr/local/viar-welcome/scripts/orphan.sh'")
        Dialog.show()
        self.show_popup("Task Done Sucessfully  ")
    def removemCache(self, widget):
        Dialog.hide()
        os.system("konsole -e 'bash /usr/local/viar-welcome/scripts/cache.sh'")
        Dialog.show()
        self.show_popup("Task Done Sucessfully  ")
    def partionManager(self, widget):
        Dialog.hide()
        os.system("partitionmanager")
        Dialog.show()
    def pamac(self, widget):
        Dialog.hide()
        os.system("pamac-manager")
        Dialog.show()
    def resetconf(self, widget):
        Dialog.hide()
        os.system("konsole -e 'bash /usr/local/viar-welcome/scripts/reset.sh'")
        self.show_popup("Reset Sucessfull please Logout & login  ")
        Dialog.show()
    def syscleaner(self, widget):
        Dialog.hide()
        os.system("sweeper")
        Dialog.show()
    def restore(self, widget):
        Dialog.hide()
        os.system("timeshift-launcher")
        Dialog.show()
    def display(self, widget):
        Dialog.hide()
        os.system("bash /usr/local/viar-welcome/scripts/display.sh")
        Dialog.show()
    def about(self, widget):
        Dialog.hide()
        subprocess.call(["python","/usr/local/viar-welcome/viar-about.py"])
        Dialog.show()
    def autostart(self, widget):
        os.system("rm -rf ~/.config/autostart/Viar-welcome.desktop")
        Dialog.close()
    def installviar(self, widget):
        Dialog.hide()
        subprocess.call(["python","/usr/local/viar-welcome/options.py"])
        Dialog.show()
       #subprocess.call(["paru","-Syyu"])

import icons


if __name__ == "__main__":
    import sys
    import subprocess
    import os
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

