#! /bin/bash

#pacman -Qq systemsettings
if pacman -Qi systemsettings &> /dev/null ; then
    kcmshell5 kcm_kscreen
fi

if pacman -Qi xfce4-settings &> /dev/null ; then
    xfce4-display-settings
fi

if pacman -Qi sway &> /dev/null ; then
    arandr
fi

if pacman -Qi qtile &> /dev/null ; then
    arandr
fi
