#!/bin/bash
echo "############################"
echo "#         Starting         #"
echo "############################"
layout=${1}
vusr=$(whoami)
### FOR KDE-Plasma

if [[ "$layout" == "viar-mak" ]] ; then
    sudo pacman -Rnsc --noconfirm viar-v11
    sudo pacman -Sy --noconfirm viar-mak
fi

if [[ $layout == "viar-v11" ]] ; then
    sudo pacman -Rnsc --noconfirm viar-mak
    sudo pacman -Sy --noconfirm viar-v11
    rm -rf /home/$vusr/.config/autostart/org.kde.latte-dock.desktop
fi

### FOR XFCE

if [[ "$layout" == "viar-xali" ]] ; then
    sudo pacman -Rnsc --noconfirm viar-xvar
    sudo pacman -Sy --noconfirm viar-xali
fi

if [[ $layout == "viar-xvar" ]] ; then
    sudo pacman -Rnsc --noconfirm viar-xali
    sudo pacman -Sy --noconfirm viar-xvar
fi

sudo cp -rf /etc/skel/. /home/$vusr/.
sudo chown -R $vusr:$vusr /home/$vusr/.

echo "############################"
echo "#            Done          #"
echo "############################"
