import QtQuick 2.0;
import calamares.slideshow 1.0;

Presentation
{
    id: presentation

    function nextSlide() {
        console.log("QML Component (default slideshow) Next slide");
        presentation.goToNextSlide();
    }

    Timer {
        id: advanceTimer
        interval: 5000
        running: presentation.activatedInCalamares
        repeat: true
        onTriggered: nextSlide()
    }

    Slide {
        anchors.fill: parent

        Image {
            id: background1
            source: "images/S1.png"
            anchors.fill: parent
        }
    }

    Slide {
        anchors.fill: parent

        Image {
            id: background2
            source: "images/S2.png"
            anchors.fill: parent
        }
    }

    Slide {
        anchors.fill: parent

        Image {
            id: background3
            source: "images/S3.png"
            anchors.fill: parent
        }
    }
    
    Slide {
        anchors.fill: parent

        Image {
            id: background4
            source: "images/S4.png"
            anchors.fill: parent
        }
    }
    
    Slide {
        anchors.fill: parent

        Image {
            id: background5
            source: "images/S5.png"
            anchors.fill: parent
        }
    }

    function onActivate() {
        console.log("QML Component (default slideshow) activated");
        presentation.currentSlide = 0;
    }

    function onLeave() {
        console.log("QML Component (default slideshow) deactivated");
    }

}
